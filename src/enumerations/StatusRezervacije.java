package enumerations;

public enum StatusRezervacije {
    naCekanju,
    preuzeta,
    spremnaZaPreuzimanje
}
